# Userscripts

Collection of my own userscripts.
Tested only with [Violentmonkey](https://violentmonkey.github.io/) on [Firefox](https://www.mozilla.org/firefox). Other Browsers or userscript managers should work, but I don't guarantee for it.

---

## How to install

If you have Violentmonkey (or other userscript manager) installed, it should usually be enough to click the link to the script you want to install in the list below.
Your userscript manager should open up and ask you if you want to install that script. Accept the installation.

If you are not asked to install the script automatically and see the source code instead, copy the URL of the page with the source code on and install the script manually via your userscript manager.
In Violentmonkey you need to open the dashboard (gear icon), then click the "+" and select "install from URL". Insert the URL you copied.

---

## Available scripts

*  [YouTV Downloader](https://gitlab.com/marcrentschler/userscripts/raw/master/scripts/youtv-downloader.user.js)