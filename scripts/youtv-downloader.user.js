// ==UserScript==
// @name          YouTV Downloader
// @description   Direkt zu der MP4-Datei von YouTV navigieren
// @author        Marc Rentschler
// @version       1.1
// @license       0BSD; https://opensource.org/licenses/0BSD
// @homepageURL   https://gitlab.com/marcrentschler/userscripts
// @supportURL    https://gitlab.com/marcrentschler/userscripts/issues
// @updateURL     https://gitlab.com/marcrentschler/userscripts/raw/master/scripts/youtv-downloader.user.js
// @namespace     *
// @include       http://youtv.de/tv-sendungen/*/*
// @include       https://youtv.de/tv-sendungen/*/*
// @include       http://www.youtv.de/tv-sendungen/*/*
// @include       https://www.youtv.de/tv-sendungen/*/*
// ==/UserScript==


if(confirm("Video in neuem Tab öffnen?") == true){
    var win = window.open($("#youtv-video source").attr("src"), '_blank');
    win.focus();
}